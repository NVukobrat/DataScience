import random
import string
import re
from nltk.stem.porter import PorterStemmer
from nltk import pos_tag


def ask(text):
    responses = []
    for sentence in getSentences(text):
        response = responde(sentence)
        responses.append(response)

    return responses


def getSentences(text):
    sentences = []
    for sentence in re.split(r"(?<!\w\.\w.)(?<![A-Z][a-z]\.)(?<=\.|\?)\s", text):
        sentences.append(sentence)

    return sentences


def responde(sentence):
    cleaned = preprocess_text(sentence)
    parsed = pos_tag(cleaned)
    print(parsed)

    pronoun, noun, adjective, verb = find_candidate_parts_of_speech(parsed)
    print('<', pronoun, noun, adjective, verb, '>')

    resp = check_for_comment_about_bot(pronoun, noun, adjective)

    if not resp:
        resp = check_for_greeting(pronoun, noun, verb)

    if not resp:
        if not pronoun:
            resp = random.choice(NONE_RESPONSES)
        elif pronoun == 'I' and not verb:
            resp = random.choice(COMMENTS_ABOUT_SELF)
        else:
            resp = construct_response(pronoun, noun, verb)

    if not resp:
        resp = random.choice(NONE_RESPONSES)

    filter_response(resp)

    return resp


def preprocess_text(sentence):
    words = sentence.split()
    print(words)
    table = str.maketrans('', '', string.punctuation)
    stripped = [w.translate(table) for w in words]
    print(stripped)
    words = [w.lower() for w in stripped if w.isalpha()]
    print(words)
    porter = PorterStemmer()
    stemmed = [porter.stem(word) for word in words]
    print(stemmed)

    return stemmed


def find_candidate_parts_of_speech(parsed_sentence):
    pronoun = None
    noun = None
    adjective = None
    verb = None

    pronoun = find_pronoun(parsed_sentence)
    noun = find_noun(parsed_sentence)
    adjective = find_adjective(parsed_sentence)
    verb = find_verb(parsed_sentence)

    return pronoun, noun, adjective, verb


def find_pronoun(sentence):
    pronoun = None

    for word, tag in sentence:
        if tag == 'PRP' and word.lower() == 'you':
            pronoun = 'I'
            break
        elif tag == 'PRP' and word.lower() == 'i':
            pronoun = 'You'
            break
        elif tag == 'PRP$' and word.lower() == 'my':
            pronoun = 'Your'
            break
        elif tag == 'PRP$' and word.lower() == 'your':
            pronoun = 'Mine'
            break
        elif tag == 'WP':
            pronoun = word

    return pronoun


def find_noun(sentence):
    noun = None

    for word, tag in sentence:
        if tag == 'NN':
            noun = word
        elif tag == 'NNS':
            noun = word
        elif tag == 'NNP':
            noun = word
        elif tag == 'NNPS':
            noun = word

    return noun


def find_adjective(sentence):
    adjective = None

    for word, tag in sentence:
        if tag == 'JJ':
            adjective = word
        if tag == 'JJR':
            adjective = word
        if tag == 'JJS':
            adjective = word

    return adjective


def find_verb(sentence):
    verb = None

    for word, tag in sentence:
        if tag == 'VB':
            verb = word
        elif tag == 'VBD':
            verb = word
        elif tag == 'VBG':
            verb = word
        elif tag == 'VBN':
            verb = word
        elif tag == 'VBP':
            verb = word
        elif tag == 'VBZ':
            verb = word
        elif tag == 'WRB':
            verb = word

    return verb


def check_for_comment_about_bot(pronoun, noun, adjective):
    resp = None
    if pronoun == 'I' and (noun or adjective):
        if noun:
            if random.choice((True, False)):
                resp = random.choice(SELF_VERBS_WITH_NOUN_CAPS_PLURAL).format(**{'noun': noun.capitalize()})
            else:
                resp = random.choice(SELF_VERBS_WITH_NOUN_LOWER).format(**{'noun': noun})
        else:
            resp = random.choice(SELF_VERBS_WITH_ADJECTIVE).format(**{'adjective': adjective})
    return resp


def check_for_greeting(pronoun, noun, verb):
    resp = ""
    if noun:
        if noun.lower() in ('hi', 'hey'):
            resp += random.choice(GREETING_NOUN)

    if verb:
        if verb.lower() in ('how', 'go', 'do'):
            resp += random.choice(GREETING_VERB)

    if pronoun and verb:
        if pronoun.lower() in ('what') and verb.lower() in ('go', 'do'):
            resp += random.choice(GREETING_AJD)

    return resp if resp != "" else None


def construct_response(pronoun, noun, verb):
    resp = []

    if pronoun:
        resp.append(pronoun)

    if verb:
        verb_word = verb[0]
        if verb_word in ('be', 'am', 'is', "'m"):
            if pronoun.lower() == 'you':
                resp.append("aren't really")
            else:
                resp.append(verb_word)
    if noun:
        pronoun = "an" if starts_with_vowel(noun) else "a"
        resp.append(pronoun + " " + noun)

    resp.append(random.choice(("tho", "bro", "lol", "bruh", "smh", "")))

    return " ".join(resp)


def starts_with_vowel(noun):
    return True if noun[0] in 'aeiou' else False


def filter_response(resp):
    filtered_resp = resp
    tokenized = resp.split(' ')
    for word in tokenized:
        if '@' in word or '#' in word or '!' in word:
            filtered_resp = "That's not nice!"
        for s in FILTER_WORDS:
            if word.lower().startswith(s):
                filtered_resp = "That's not nice!"

    return filtered_resp


# Template for responses that include a direct noun which is indefinite/uncountable
NONE_RESPONSES = [
    "I don't understand. Can you explain? :)"
]

COMMENTS_ABOUT_SELF = [
    "Nice :D"
]

SELF_VERBS_WITH_NOUN_CAPS_PLURAL = [
    "My last startup totally crushed the {noun} vertical!",
    "Were you aware I was a serial entrepreneur in the {noun} sector?",
    "My startup is Uber for {noun}",
    "I really consider myself an expert on {noun}",
]

SELF_VERBS_WITH_NOUN_LOWER = [
    "Yeah but I know a lot about {noun}",
    "My bros always ask me about {noun}",
]

SELF_VERBS_WITH_ADJECTIVE = [
    "I'm personally building the {adjective} Economy",
    "I consider myself to be a {adjective}preneur",
]

GREETING_NOUN = [
    "Hey man :D ",
    "Hi to you too :D "
]

GREETING_VERB = [
    "Nice, you? :) ",
    'Excellent! :D '
]

GREETING_AJD = [
    "Nothing special :D You? ",
    "Nice of you to ask, really good :D "
]

FILTER_WORDS = [
    "beeyotch",
    "biatch",
    "bitch",
    "chinaman",
    "chinamen",
    "chink",
    "crazie",
    "crazy",
    "crip",
    "cunt",
    "dago",
    "daygo",
    "dego",
    "dick",
    "dumb",
    "douchebag",
    "dyke",
    "fag",
    "fatass",
    "fatso",
    "gash",
    "gimp",
    "golliwog",
    "gook",
    "gyp",
    "halfbreed",
    "half-breed",
    "homo",
    "hooker",
    "idiot",
    "insane",
    "insanitie",
    "insanity",
    "jap",
    "kike",
    "kraut",
    "lame",
    "lardass",
    "lesbo",
    "lunatic",
    "negro",
    "nigga",
    "nigger",
    "nigguh",
    "paki",
    "pickaninnie",
    "pickaninny",
    "pussie",
    "pussy",
    "raghead",
    "retard",
    "shemale",
    "skank",
    "slut",
    "spade",
    "spic",
    "spook",
    "tard",
    "tits",
    "titt",
    "trannie",
    "tranny",
    "twat",
    "wetback",
    "whore",
    "wop"
]



# TEST #

# res = ask("Hi, my name is Nikola :D What is your name? And how are you? :D")
# print(res)
# res = ask("I am nice person.")
# print(res)
# res = ask("Can you help me?")
# print(res)
# res = ask("You are nice person :)")
# rint(res)
res = ask("Hey, Hey man, or Hi")
print(res)
res = ask("How?s it going? or How are you doing?")
print(res)
res = ask("What?s up?, What?s new?, or What?s going on?")
print(res)
res = ask(" How?s everything ?, How are things?, or How?s life?")
print(res)
res = ask("How?s your day? or How?s your day going?")
print(res)
res = ask("Good to see you or Nice to see you")
print(res)
res = ask("Long time no see or It?s been a while")
print(res)
