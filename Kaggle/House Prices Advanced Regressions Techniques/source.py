import csv
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn import linear_model
from sklearn import svm
from sklearn.preprocessing import LabelEncoder


def loadAnalyticData(path):
    """
    Loads csv data and converts it to dictionary which can be
    accessed with headers. Also watch on data types. If it's an number
    in dictionary will be saved as number.

    :param path: Path to CSV file.
    :return: Data dictionary.
    """
    with open(path) as data:
        reader = csv.reader(data)

        headers = next(reader)

        column = {}
        for header in headers:
            column[header] = []

        for row in reader:
            for header, value in zip(headers, row):
                if value.isdigit():
                    value = float(value)
                column[header].append(value)

    return column


def initialTrainTestSplit(data):
    """
    Splits dictionary into train and test dictionaries.

    :param data: Full data-set.
    :return: Train and Test data.
    """
    train = {}
    test = {}
    for key in data:
        percent = int(0.8 * len(data[key]))
        train[key] = data[key][:percent]
        test[key] = data[key][percent:]

    return train, test


def plot2DFeatures(data, firstKey, secondKey):
    """
    Plots 2D data. Works by providing dictionary data to function,
    and afterwards two dictionary keys that needs to be plotted.

    :param data: Dictionary with data.
    :param firstKey: First dictionary key.
    :param secondKey: Second dictionary key.
    """
    plt.plot(data[firstKey], data[secondKey], 'ro')
    plt.xlabel(firstKey)
    plt.ylabel(secondKey)
    plt.show()


def convertToPandasDataFrame(data):
    """
    Converts Python dictionary to Pandas DataFrame.

    :param data: Dictionary.
    :return: DataFrame
    """
    data = pd.DataFrame(data)

    return data


def splitToFeaturesAndPredictions(data):
    """
    Split data-set to features X and predictions y.

    :param data: Data-set.
    :return: Features, Predictions.
    """
    X = data.drop("SalePrice", axis=1)
    y = data["SalePrice"].values

    return X, y


def encodeFeatures(features):
    """
    Encodes String type features from data-set. Encodes them
    into Integer values so model can parse them.

    :param features: Data-set features.
    """
    for row in features:
        if row == "Id":
            continue
        features[row] = LabelEncoder().fit_transform(features[row].astype(str))


def score(real, predicted):
    """
    Returns Root Mean Squared Logarithmic Error score.

    :param real: Real answers.
    :param predicted: Predicted answers.
    :return: Score.
    """
    sum = 0.0
    for x in range(len(predicted)):
        if predicted[x] < 0 or real[x] < 0:
            continue
        p = np.log(predicted[x] + 1)
        r = np.log(real[x] + 1)
        sum = sum + (p - r) ** 2

    return (sum / len(predicted)) ** 0.5


def writeResultsToFile(ids, predictions, header):
    """
    Write final results to file.

    :param ids: Unique ID's from elements.
    :param predictions: Predictions value of element.
    :param header: Description Header of data-set.
    """
    with open('results.csv', 'w') as file:
        file.write(header)
        for i in range(len(ids)):
            line = str(int(ids[i])) + ',' + str(predictions[i]) + '\n'
            file.write(line)


def initialPredictions(models):
    """
    Initial data analysis facade for finding best model.

    :param models: List of models.
    :return: Best fitting model.
    """
    data = loadAnalyticData("data/train.csv")
    train, test = initialTrainTestSplit(data)

    best = (None, 1)
    for model in models:
        trainFacade(model, train)
        result = testFacade(model, test)

        if float(result) < best[1]:
            best = (model, float(result))

    print("Best model is " + best[0].__class__.__name__)
    return best[0]


def trainFacade(model, data):
    """
    Facade for model training flow.

    :param model: Learning model.
    """
    data = convertToPandasDataFrame(data)
    X_train, y_train = splitToFeaturesAndPredictions(data)
    encodeFeatures(X_train)
    model.fit(X_train, y_train)


def testFacade(model, data):
    """
    Facade for model testing flow.

    :param model: Learning model.
    """
    data = convertToPandasDataFrame(data)
    X_test, y_test = splitToFeaturesAndPredictions(data)
    encodeFeatures(X_test)
    prediction = model.predict(X_test)

    modelName = model.__class__.__name__
    result = str(score(y_test, prediction))

    print(modelName + " scored " + result)

    return result


def finalPredictions(model):
    """
    Final prediction operations facade.

    :param model: Best fitting model.
    """
    trainFinalFacade(model)
    testFinalFacade(model)


def trainFinalFacade(model):
    """
    Facade for model training flow.

    :param model: Learning model.
    """
    data = loadAnalyticData("data/train.csv")
    data = convertToPandasDataFrame(data)
    X_train, y_train = splitToFeaturesAndPredictions(data)
    encodeFeatures(X_train)
    model.fit(X_train, y_train)


def testFinalFacade(model):
    """
    Facade for model testing flow.

    :param model: Learning model.
    """
    X_test = loadAnalyticData("data/test.csv")
    X_test = convertToPandasDataFrame(X_test)
    encodeFeatures(X_test)
    prediction = model.predict(X_test)
    writeResultsToFile(X_test["Id"], prediction, "Id,SalePrice\n")

    # plt.plot(X_test, prediction, color="blue", linewidth=3)
    # plt.show()


def main():
    """
    Starting function.
    """
    models = [linear_model.LinearRegression(), svm.SVR(), linear_model.Lasso(), linear_model.LinearRegression()]

    bestModel = initialPredictions(models)
    finalPredictions(bestModel)


main()
