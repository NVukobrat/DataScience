from FaceDetection.classifier import Classifier
from FaceDetection.clf_utils import CLFUtils
from FaceDetection.image_reader import ImageReader

images = ImageReader.readImages("data")
CLFUtils.preTrainCLF(images)

Classifier.train("labels.csv", "reps.csv")
prediction = Classifier.predict(images[25][0])
print(prediction)

#TODO: Refactor to work with multi-face from one image