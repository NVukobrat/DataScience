import numpy as np
import pandas as pd

from FaceDetection.clf_utils import CLFUtils
from FaceDetection.image_representation import ImageRepresentation


class Classifier:
    clf = CLFUtils.loadCLF()
    predictions = []

    @staticmethod
    def train(labelsCSV, repsCSV):
        fname = labelsCSV
        labels = pd.read_csv(fname, header=None).as_matrix().flatten()

        fname = repsCSV
        embeddings = pd.read_csv(fname, header=None).as_matrix()

        Classifier.clf.fit(embeddings, labels)
        CLFUtils.serializeCLF(Classifier.clf)

    @staticmethod
    def predict(imagePath):
        Classifier.predictions.clear()
        reps = ImageRepresentation.getRep(imagePath=imagePath)

        if reps is None:
            return None

        prediction = Classifier.clf.predict_proba(np.asmatrix(reps))
        Classifier.predictions.append(prediction)

        return Classifier.predictions


