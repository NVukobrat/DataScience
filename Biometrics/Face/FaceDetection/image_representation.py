import cv2
import openface


class Data:
    reps = []


class Config:
    Image_Dimension = 96


class ImageRepresentation:
    align = openface.AlignDlib(
        facePredictor="shape_predictor_68_face_landmarks.dat/data"
    )
    net = openface.TorchNeuralNet(
        model="openface/models/openface/nn4.small2.v1.t7",
        imgDim=Config.Image_Dimension,
        cuda=False  # Set to true to force Nvidia GPU usage
    )

    @staticmethod
    def getRep(imagePath):
        Data.reps.clear()

        bgrImage = cv2.imread(imagePath)
        rgbImage = cv2.cvtColor(bgrImage, cv2.COLOR_BGR2RGB)

        faces = ImageRepresentation.align.getAllFaceBoundingBoxes(rgbImage)

        rep = None
        for faceRect in faces:
            alignFace = ImageRepresentation.align.align(
                imgDim=Config.Image_Dimension,
                rgbImg=rgbImage,
                bb=faceRect,
                landmarkIndices=openface.AlignDlib.OUTER_EYES_AND_NOSE
            )
            rep = ImageRepresentation.net.forward(alignFace)
            Data.reps.append(rep)

        return rep

