import pickle
from pathlib import Path

import numpy
from sklearn.svm import SVC

from FaceDetection.image_representation import ImageRepresentation


class CLFUtils:
    pickleFileName = "classifier.pkl"
    @staticmethod
    def serializeCLF(clf):
        serializedFile = Path(CLFUtils.pickleFileName)

        if serializedFile.exists():
            pass
        else:
            with open(CLFUtils.pickleFileName, 'wb') as file:
                pickle.dump(clf, file)

    @staticmethod
    def loadCLF():
        clf = None
        serializedFile = Path(CLFUtils.pickleFileName)

        if serializedFile.exists():
            with open(CLFUtils.pickleFileName, 'rb') as file:
                clf = pickle.load(file)
        else:
            clf = SVC(C=1, kernel='linear', probability=True)

        return clf

    @staticmethod
    def preTrainCLF(images):
        serializedFile = Path(CLFUtils.pickleFileName)

        if serializedFile.exists():
            return

        imagesNum = len(images)
        parsedNum = 1

        labelsText = ""
        repsText = []

        for imagePath, imageClass in images:
            rep = ImageRepresentation.getRep(imagePath=imagePath)

            if rep is None:
                continue

            labelsText += imageClass + "\n"
            repsText.append(rep)

            print("Parsed {}/{} images.".format(parsedNum, imagesNum))
            parsedNum += 1

        # Save reps and labels to csv files
        with open('labels.csv', 'w') as fp:
            fp.write(labelsText)
            numpy.savetxt('reps.csv', repsText, delimiter=',')
