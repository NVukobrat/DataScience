import os


class ImageReader:
    @staticmethod
    def readImages(file_path):
        images = []
        for image_folder in os.listdir(file_path):
            for image in os.listdir(file_path + "/" + image_folder):
                imagePath = file_path + "/" + image_folder + "/" + image
                images.append([imagePath, image_folder])

        return images