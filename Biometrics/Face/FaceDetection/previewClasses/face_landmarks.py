import dlib
from skimage import io


class FaceLandmarks:
    @staticmethod
    def detect(image, detected_faces):
        predictor_model = "shape_predictor_68_face_landmarks.dat/data"
        face_pose_predictor = dlib.shape_predictor(predictor_model)

        pose_landmarks_for_face = []
        for face_rect in detected_faces:
            pose_landmarks_for_face.append(face_pose_predictor(image, face_rect))

            # Show result
            # pose_landmarks = face_pose_predictor(image, face_rect)
            # win = dlib.image_window()
            # win.set_image(image)
            # win.add_overlay(face_rect)
            # win.add_overlay(pose_landmarks)
            # dlib.hit_enter_to_continue()

        return pose_landmarks_for_face
