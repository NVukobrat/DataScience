import dlib
from skimage import io


class FaceFinder:
    @staticmethod
    def detect(image):
        face_detector = dlib.get_frontal_face_detector()

        detected_faces = face_detector(image, 1)

        # Show result
        # win = dlib.image_window()
        # win.set_image(image)
        # win.add_overlay(detected_faces)
        # dlib.hit_enter_to_continue()

        return detected_faces

