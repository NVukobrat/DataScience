import os

import cv2
import openface

from FaceDetection.preview_classes.face_finder import FaceFinder


class FaceTransform:
    y = 0

    @staticmethod
    def transform(image, type):
        image = cv2.imread(image)

        detected_faces = FaceFinder.detect(image)
        predictor_model = "shape_predictor_68_face_landmarks.dat/data"
        face_aligner = openface.AlignDlib(predictor_model)

        for i, face_rect in enumerate(detected_faces):
            alignedFace = face_aligner.align(80, image, face_rect,
                                             landmarkIndices=openface.AlignDlib.OUTER_EYES_AND_NOSE)

            # Show result
            # win = dlib.image_window()
            # win.set_image(alignedFace)
            # dlib.hit_enter_to_continue()

            path = "parsedData/" + type + "/"
            if not os.path.exists(path):
                os.mkdir(path)

            name = "aligned_face_{}.jpg".format(FaceTransform.y)
            cv2.imwrite(path + name, alignedFace)
            FaceTransform.y += 1
            print(FaceTransform.y, type, path)
