import os


def read_images(root_path):
    images = []

    for images_folder in os.listdir(root_path):
        for image in os.listdir(root_path + "/" + images_folder):
            fullImagePath = root_path + "/" + images_folder + "/" + image
            images.append([fullImagePath, images_folder])

    return images
