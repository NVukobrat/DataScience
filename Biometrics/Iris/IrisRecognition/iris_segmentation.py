import random
import string

import cv2
import numpy as np
import bitarray

from IrisRecognition.ReedSolomonCode.reedsolo import RSCodec
from IrisRecognition.read_images import read_images

np.set_printoptions(threshold=np.nan)

# GLOBAL VARIABLES
#####################################
# Holds the current element of the image used by the getNewEye function
currentEye = 0
# Represents index of list when read from read_images.py
EYE_IMAGE_PATH = 0
EYE_IMAGE_CLASS = 1
# Pupil centroids for current eye image.
pupilCentroids = (0, 0)
# Iris radius
irisRadius = 0
# Generated private key for current Iris
currentPrivateKey = None
# Added 0 to begin of key for XORing
addedLen = 0
#####################################


# Returns different image filename on each call.
#
# @param list       List of images filename.
# @return string    Next image filename.
def getNewEye(imageList):
    global currentEye
    newEye = imageList[currentEye]
    currentEye += 1
    return newEye


# Marks pupil of eye and sets pupil coordinates.
#
# @param eyeImage     Image of eye.
# @return imageGray   Image in Gray spectrum with marked pupil.
def getPupil(eyeImage):
    imageCopy = eyeImage.copy()
    imageGray = cv2.cvtColor(imageCopy, cv2.COLOR_BGR2GRAY)

    thresh = cv2.inRange(imageGray, 60, 255)
    image, contours, hierarchy = cv2.findContours(thresh, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)

    for i in range(len(contours)):
        currentContour = contours[i]
        moments = cv2.moments(currentContour)
        area = moments['m00']

        if 500 < area < 30000:
            x, y = getMomentsCentroids(moments)
            cv2.ellipse(imageGray, (x, y), (12, 12), 0, 0, 360, 0, cv2.FILLED)
            global pupilCentroids
            pupilCentroids = (int(x), int(y))

    return imageGray


# Return centroids of image moments.
#
# @param moments  Moments of image.
# @return cx, cy
def getMomentsCentroids(moments):
    area = moments['m00']
    cx = int(moments['m10'] / area)
    cy = int(moments['m01'] / area)

    return cx, cy


# Detect iris boundaries and crop iris image.
#
# @parm eyeImage    Image with pupil.
# @return irisImage Cropped iris with pupil.
def getIris(eyeImage):
    resultImage = None
    imageCopy = eyeImage.copy()
    cv2.blur(imageCopy, (15, 15))

    circles = cv2.HoughCircles(imageCopy, cv2.HOUGH_GRADIENT, 1.2, 100)

    if circles is not None:
        circles = np.round(circles[0, :]).astype("int")

        for (x, y, r) in circles:
            global irisRadius
            irisRadius = r

            cv2.circle(imageCopy, (x, y), r, (0, 255, 0), 3)
            mask = np.zeros(imageCopy.shape, dtype=np.uint8)
            cv2.circle(mask, (x, y), r, (255, 255, 255), -1, 8, 0)

            resultImage = imageCopy & mask
            resultImage = resultImage[(y - r):(y + r), (x - r):(x + r)]

    return resultImage


# Transform Iris from Polar to Cartasian coordinates.
# This is normalization step, also, during the normalization
# black part of iris image is removed(it is created during
# the iris cropping).
#
# @parm irisImage       Cropped Iris image.
# @return normalized    Normalized Iris image.
def getPolarToCartCoor(irisImage):
    normalized = None

    if irisImage is not None and np.any(irisImage <= 0):
        irisFloat = irisImage.copy().astype(np.float64)
        x, y = irisFloat.shape
        xCar = ((x / 2.0) ** 2)
        yCar = ((y / 2.0) ** 2)
        z = np.sqrt(xCar + yCar)

        normalized = cv2.linearPolar(irisFloat, (x / 2, y / 2), z, cv2.WARP_FILL_OUTLIERS)
        normalized = normalized / 255
        normalized = normalized[0:, 0:int(normalized.shape[1] * 0.67)]
        normalized = np.rot90(normalized, 3)

    return normalized


# Show image for preview.
#
# @parm image       Image to display
def showImage(image, winName):
    if image is not None and np.any(image <= 0):
        cv2.imshow(winName, image)
        cv2.waitKey(10)


# Build Gabor filter bank.
#
# @return   Bank of Gabor filters.
def buildFilters():
    filters = []
    ksize = 31

    for theta in np.arange(0, np.pi, np.pi / 16):
        kern = cv2.getGaborKernel((ksize, ksize), 4.0, theta, 10.0, 0.5, 0, ktype=cv2.CV_32F)
        kern /= 1.5 * kern.sum()
        filters.append(kern)

    return filters


# Extract features from Iris image.
#
# @parm normalizedImage     Normalized Iris image.
# @parm filters             Gabor filter bank.
# @return                   Filtered iris with most detail number.
def processImage(normalizedImage, filters):
    accum = np.zeros_like(normalizedImage)

    for kern in filters:
        filteredImage = cv2.filter2D(normalizedImage, cv2.CV_8UC3, kern)
        np.maximum(accum, filteredImage, accum)

    return accum


# Get Iris binary template.
#
# @parm filteredImage       Image with Gabor filter applied.
# @return binaryTemplate    Binary array of forwarded Iris image.
def getIrisTemplate(filteredImage):
    binaryTemplate = filteredImage.astype(np.int).flatten()

    return binaryTemplate


# Generate private binary array key for XOR operation with Iris template.
#
# @parm length          Key length
# @parm binaryTemplate  Iris binary template
# @return binaryArray   Private key
def generatePrivateKey(length):
    binaryArrayConverter = bitarray.bitarray()
    binaryArray = []

    randomKey = ''.join(random.choices(string.ascii_uppercase + string.digits, k=length))
    binaryArrayConverter.frombytes(randomKey.encode("utf-8"))

    for i in binaryArrayConverter:
        binaryArray.append(int(i))

    return binaryArray


# Encrypt iris tempalte with random generated key.
# Use bitwise XORing for encrypting iris tamplate with key. For purpose
# of XOR key and iris must be at same length, so at the MSB key position
# needed 0 values are added. Number of added 0's are stored.
#
# @parm iris            Binary iris template.
# @parm key             Random generated key.
# @return privateKey    Private XOR-ed Iris key.
def encryptTemplate(iris, key):
    irisLength = len(iris)

    if len(iris) < len(key):
        print("Low quality iris template. Passing!")
    else:
        global addedLen
        addedLen = irisLength - len(key)
        for i in range(addedLen):
            key.insert(0, 0)

    privateKey = iris ^ key
    global currentPrivateKey
    currentPrivateKey = privateKey

    return privateKey

# Decrypt iris template using stored private key.
# Uses stored added number of 0's to MSB of key to POP them from
# key.

# @parm iris            Binary iris template.
# @parm key             Stored key.
# @return originalKey   Original generated key.
def decryptTemplate(iris, key):
    originalKey = iris ^ key
    originalKey = originalKey.tolist()

    global addedLen
    for i in range(addedLen):
        originalKey.pop(0)

    return originalKey


# Reed-Solomon Error Correction Encoder.
# Encodes generated key for purpose of correcting minimal IRIS
# deviation.
#
# @parm generatedKey    Random generated key.
# @return encoded       ECC encoded key.
def reedSolomonEncode(generatedKey):
    reedSolomon = RSCodec(10)
    encoded = reedSolomon.encode(generatedKey)

    return encoded


# Reed-Solomon Error Correction Decoder.
# Decodes generated key that was previously encoded with ECC code.
#
# @parm encodedKey      Encoded key.
# @return decoded       Decoded key.
def reedSolomonDecode(encodedKey):
    reedSolomon = RSCodec(10)
    decoded = reedSolomon.decode(encodedKey)

    return decoded


# Preview windows
cv2.namedWindow("Original", cv2.WINDOW_NORMAL)
cv2.namedWindow("Iris", cv2.WINDOW_NORMAL)
cv2.namedWindow("Normalized", cv2.WINDOW_NORMAL)
cv2.namedWindow("Filtered", cv2.WINDOW_NORMAL)

eyesList = read_images("dataset/Sessao_1")
filters = buildFilters()

for i in range(len(eyesList)):
    eyeLabelAndPath = eyesList[i]
    eyeImage = cv2.imread(str(eyeLabelAndPath[EYE_IMAGE_PATH]), cv2.IMREAD_COLOR)
    pupil = getPupil(eyeImage)
    iris = getIris(pupil)

    if iris is not None and np.any(iris <= 0):
        showImage(eyeImage, "Original")
        showImage(iris, "Iris")
        normalized = getPolarToCartCoor(iris)
        showImage(normalized, "Normalized")

        filteredImage = processImage(normalized, filters)
        showImage(filteredImage, "Filtered")

        binaryTemplate = getIrisTemplate(filteredImage)
        randomKey = generatePrivateKey(254)
        encodedKey = reedSolomonEncode(randomKey)
        encryptedKey = encryptTemplate(binaryTemplate, encodedKey)

        decryptedKey = decryptTemplate(binaryTemplate, encryptedKey)
        decodedKey = reedSolomonDecode(decryptedKey)

        if np.array_equal(decodedKey, randomKey):
            print("NICE")
        else:
            print("PFFF")

        if i == 0:
            break

